//ios 默认的UISwipeGestureRecogner只支持单方向的swipe，为了实现双向swipe 需要在 初始化view中添加如下代码
 
 UISwipeGestureRecognizer *swipe = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)]autorelease];
[self.view addGestureRecognizer:swipe];
 
 UISwipeGestureRecognizer *swipeleft = [[[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)]autorelease];
[self.view addGestureRecognizer:swipeleft];
  swipeleft.direction = UISwipeGestureRecognizerDirectionLeft;
 
//将响应处理函数绑定到同一个函数来处理....
 
- (void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer 
{
        NSLog(@"Swipe");
}